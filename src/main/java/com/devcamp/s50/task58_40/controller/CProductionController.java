package com.devcamp.s50.task58_40.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.s50.task58_40.model.CProduction;
import com.devcamp.s50.task58_40.repository.IProductionRepository;


@CrossOrigin
@RestController
@RequestMapping("/")
public class CProductionController {
    @Autowired
    IProductionRepository iProductionRepository ;

    @GetMapping("/production")
    public ResponseEntity<List<CProduction>> getAllProduction(){
        try{
            List<CProduction> ListProduction = new ArrayList<CProduction>();

            iProductionRepository.findAll().forEach(ListProduction::add);
            return new ResponseEntity<List<CProduction>>(ListProduction, HttpStatus.OK);
        }catch(Exception ex){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);    
        }
    }
}
