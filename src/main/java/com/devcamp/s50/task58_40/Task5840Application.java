package com.devcamp.s50.task58_40;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task5840Application {

	public static void main(String[] args) {
		SpringApplication.run(Task5840Application.class, args);
	}

}
