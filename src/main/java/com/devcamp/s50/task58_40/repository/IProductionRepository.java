package com.devcamp.s50.task58_40.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.s50.task58_40.model.CProduction;

public interface IProductionRepository extends JpaRepository<CProduction, Long>{
    
}
